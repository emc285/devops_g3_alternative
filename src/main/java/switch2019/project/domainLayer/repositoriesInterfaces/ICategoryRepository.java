package switch2019.project.domainLayer.repositoriesInterfaces;

import org.springframework.stereotype.Repository;
import switch2019.project.domainLayer.domainEntities.aggregates.category.Category;
import switch2019.project.domainLayer.domainEntities.vosShared.CategoryID;
import switch2019.project.infrastructureLayer.dataPersistence.dataModel.CategoryJpa;

import java.util.List;
import java.util.Optional;

/**
 * The interface Category repository.
 */
@Repository
public interface ICategoryRepository {

    //----------------------- NOVO ----------------------//

    Category save(Category category );

    Optional<Category> findById(String id, String denomination);

    boolean existsById(CategoryID categoryID);

    long count();

    List<Category> findAll();

    void delete(CategoryID categoryID);

    List<Category> findAllById(String id, String denomination);
}
