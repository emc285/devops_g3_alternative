package switch2019.project.controllerLayer.controllers.controllersREST;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import switch2019.project.applicationLayer.applicationServices.US005_1CreateGroupCategoryService;
import switch2019.project.applicationLayer.dtos.CreateGroupCategoryDTO;
import switch2019.project.applicationLayer.dtos.GroupDTO;
import switch2019.project.applicationLayer.dtos.NewGroupCategoryInfoDTO;
import switch2019.project.applicationLayer.dtosAssemblers.CreateGroupCategoryDTOAssembler;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
public class US005_1CreateGroupCategoryControllerREST {

    @Autowired
    private US005_1CreateGroupCategoryService service;

    //US005.1 Como responsável de grupo, quero criar categoria e associá-la ao grupo.

    @PostMapping("/persons/{personEmail}/groups/{groupDenomination}/categories")
    public ResponseEntity<Object> createGroupCategory(@RequestBody NewGroupCategoryInfoDTO info,
                                                      @PathVariable final String personEmail,
                                                      @PathVariable final String groupDenomination) {

        CreateGroupCategoryDTO createGroupCategoryDTO = CreateGroupCategoryDTOAssembler.createDTOFromPrimitiveTypes(personEmail, groupDenomination, info.getCategoryDenomination());

        GroupDTO result = service.createCategoryAsPeopleInCharge(createGroupCategoryDTO);

        Link link_to_admins = linkTo(methodOn(US002_1CreateGroupControllerREST.class).getGroupAdmins(groupDenomination)).withRel("admins");
        Link link_to_members = linkTo(methodOn(US002_1CreateGroupControllerREST.class).getGroupMembers(groupDenomination)).withRel("members");
        Link link_to_ledger = linkTo(methodOn(US002_1CreateGroupControllerREST.class).getGroupLedger(groupDenomination)).withRel("ledger");
        Link link_to_accounts = linkTo(methodOn(US002_1CreateGroupControllerREST.class).getGroupAccounts(personEmail, groupDenomination)).withRel("accounts");
        Link link_to_categories = linkTo(methodOn(US002_1CreateGroupControllerREST.class).getGroupCategories(personEmail,groupDenomination)).withRel("categories");

        result.add(link_to_admins);
        result.add(link_to_members);
        result.add(link_to_ledger);
        result.add(link_to_accounts);
        result.add(link_to_categories);

        return new ResponseEntity<>(result, HttpStatus.CREATED);
    }
}
