package switch2019.project.controllerLayer.controllers.controllersREST;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import switch2019.project.applicationLayer.applicationServices.US003AddPersonToGroupService;
import switch2019.project.applicationLayer.dtos.AddPersonToGroupDTO;
import switch2019.project.applicationLayer.dtos.GroupDTO;
import switch2019.project.applicationLayer.dtos.NewAddPersonToGroupInfoDTO;
import switch2019.project.applicationLayer.dtosAssemblers.AddPersonToGroupDTOAssembler;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

/**
 * ControllerRESTUS003
 */
//US03. Como gestor de sistema, quero acrescentar pessoas ao grupo.

@RestController
public class US003AddPersonToGroupControllerREST {

    @Autowired
    private US003AddPersonToGroupService serviceUS003;


    /**
     * Add person to group 2 response entity.
     *
     * @param info the info
     * @return the response entity
     */



    @PostMapping("/groups/{denomination}/members")
    public ResponseEntity<Object> addPersonToGroupP(@RequestBody NewAddPersonToGroupInfoDTO info,
                                                    @PathVariable final String denomination
    ) {

        AddPersonToGroupDTO addPersonToGroupDTO = AddPersonToGroupDTOAssembler.createDataTransferObject_Primitives(info.getEmail(),denomination);

//        The new TDO to be used is addPersonToGroupDTOInput
        GroupDTO result = serviceUS003.addPersonToGroup(addPersonToGroupDTO);

        Link link_to_admins = linkTo(methodOn(US002_1CreateGroupControllerREST.class).getGroupAdmins(denomination)).withRel("admins");
        Link link_to_members = linkTo(methodOn(US002_1CreateGroupControllerREST.class).getGroupMembers(denomination)).withRel("members");
        Link link_to_ledger = linkTo(methodOn(US002_1CreateGroupControllerREST.class).getGroupLedger(denomination)).withRel("ledger");
        Link link_to_accounts = linkTo(methodOn(US002_1CreateGroupControllerREST.class).getGroupAccounts(info.getEmail(), denomination)).withRel("accounts");
        Link link_to_categories = linkTo(methodOn(US002_1CreateGroupControllerREST.class).getGroupCategories(info.getEmail(),denomination)).withRel("categories");

        result.add(link_to_admins);
        result.add(link_to_members);
        result.add(link_to_ledger);
        result.add(link_to_accounts);
        result.add(link_to_categories);


        return new ResponseEntity<>(result, HttpStatus.CREATED);
    }


}
