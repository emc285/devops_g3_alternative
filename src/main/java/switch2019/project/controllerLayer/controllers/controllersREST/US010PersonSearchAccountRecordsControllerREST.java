package switch2019.project.controllerLayer.controllers.controllersREST;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import switch2019.project.applicationLayer.applicationServices.CreatePersonService;
import switch2019.project.applicationLayer.applicationServices.US010PersonSearchAccountRecordsService;
import switch2019.project.applicationLayer.dtos.PersonEmailDTO;
import switch2019.project.applicationLayer.dtos.PersonSearchAccountRecordsInDTO;
import switch2019.project.applicationLayer.dtos.SearchAccountRecordsOutDTO;
import switch2019.project.applicationLayer.dtos.TransactionsDTO;
import switch2019.project.applicationLayer.dtosAssemblers.PersonSearchAccountRecordsInDTOAssembler;

@RestController
public class US010PersonSearchAccountRecordsControllerREST {

    @Autowired
    private US010PersonSearchAccountRecordsService searchPersonAccountRecordsService;
    @Autowired
    private CreatePersonService personRecordsService;

    //Search account records within a period of dates
    @GetMapping("/persons/{personID}/ledgers/records")
    public ResponseEntity<Object> searchPersonRecords(@RequestParam(value = "accountName", defaultValue = "") String accountDenomination,
                                                      @RequestParam(value = "startDate", defaultValue = "") String startDate,
                                                      @RequestParam(value = "endDate", defaultValue = "") String endDate,
                                                      @PathVariable(value = "personID") String personEmail) {

        //DTO for passing info to service that searches account records within period
        PersonSearchAccountRecordsInDTO personSearchAccountRecordsInDTO = PersonSearchAccountRecordsInDTOAssembler.personAccountTransactionsInDTO(personEmail, accountDenomination, startDate, endDate);

        //Info to return whether form is empty or not
        boolean searchFormEmpty = (accountDenomination.isEmpty() && startDate.isEmpty() && endDate.isEmpty());

        if (!searchFormEmpty) {

            SearchAccountRecordsOutDTO searchResult = searchPersonAccountRecordsService.getPersonAccountTransactionsWithinPeriod(personSearchAccountRecordsInDTO);

            return new ResponseEntity<>(searchResult, HttpStatus.OK);

        } else {

            PersonEmailDTO personEmailDTO = new PersonEmailDTO(personEmail);
            TransactionsDTO defaultResult = personRecordsService.getPersonLedger(personEmailDTO);

            return new ResponseEntity<>(defaultResult, HttpStatus.OK);
        }
    }

}
