package switch2019.project.controllerLayer.controllers.controllersCLI;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import switch2019.project.applicationLayer.applicationServices.US012GroupTransactionsWithinPeriodService;
import switch2019.project.applicationLayer.dtos.GroupTransactionsWithinPeriodDTOin;
import switch2019.project.applicationLayer.dtos.GroupTransactionsWithinPeriodDTOout;
import switch2019.project.applicationLayer.dtosAssemblers.GroupTransactionsWithinPeriodDTOinAssembler;

import java.time.LocalDate;

/**
 * US012 - As a member of a given group, I want to obtain the transactions of that group within a given period
 */
@Controller
public class US012GroupTransactionsWithinPeriodController {

    @Autowired
    private US012GroupTransactionsWithinPeriodService us012GroupTransactionsWithinPeriodService;

    /**
     * Instantiates a new Us 012 group transactions within period controller.
     *
     * @param us012GroupTransactionsWithinPeriodService the us 012 group transactions within period service
     */
    public US012GroupTransactionsWithinPeriodController(US012GroupTransactionsWithinPeriodService us012GroupTransactionsWithinPeriodService) {
        this.us012GroupTransactionsWithinPeriodService = us012GroupTransactionsWithinPeriodService;
    }

    /**
     * Gets group transactions within period dt oout.
     *
     * @param personEmail       the person email
     * @param groupDenomination the group denomination
     * @param startDate         the start date
     * @param endDate           the end date
     * @return the group transactions within period dt oout
     */
    public GroupTransactionsWithinPeriodDTOout getGroupTransactionsWithinPeriodDTOout(String personEmail, String groupDenomination, LocalDate startDate, LocalDate endDate) {
        GroupTransactionsWithinPeriodDTOin groupTransactionsWithinPeriodDTOin = GroupTransactionsWithinPeriodDTOinAssembler.createGroupTransactionsWithinPeriodDTOin(personEmail, groupDenomination, startDate, endDate);
        return us012GroupTransactionsWithinPeriodService.getGroupTransactionsWithinPeriod(groupTransactionsWithinPeriodDTOin);
    }
}
