package switch2019.project.applicationLayer.dtosAssemblers;

import switch2019.project.applicationLayer.dtos.GroupAllMembersDTO;
import switch2019.project.applicationLayer.dtos.GroupMemberClearanceDTO;

import java.util.List;

public class GroupAllMembersDTOAssembler {

    public static GroupAllMembersDTO createDTOFromDomainObject(List<GroupMemberClearanceDTO> allMembers) {

        GroupAllMembersDTO groupAllMembersDTO = new GroupAllMembersDTO(allMembers);
        return groupAllMembersDTO;
    }
}
