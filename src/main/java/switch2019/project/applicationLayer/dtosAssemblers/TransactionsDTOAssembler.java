package switch2019.project.applicationLayer.dtosAssemblers;

import switch2019.project.applicationLayer.dtos.TransactionDTOout;
import switch2019.project.applicationLayer.dtos.TransactionsDTO;

import java.util.List;

public class TransactionsDTOAssembler {

    public static TransactionsDTO createDTOFromPrimitiveTypes(List<TransactionDTOout> transactions) {

        TransactionsDTO transactionsDTO = new TransactionsDTO(transactions);
        return transactionsDTO;
    }
}
