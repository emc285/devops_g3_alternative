package switch2019.project.applicationLayer.dtosAssemblers;

import switch2019.project.applicationLayer.dtos.AccountDTO;
import switch2019.project.applicationLayer.dtos.AccountsDTO;

import java.util.List;

public class AccountsDTOAssembler {

    public static AccountsDTO createDTOFromDomainObject(List<AccountDTO> accountDTOS) {

        AccountsDTO accountsDTO = new AccountsDTO(accountDTOS);

        return accountsDTO;
    }
}
