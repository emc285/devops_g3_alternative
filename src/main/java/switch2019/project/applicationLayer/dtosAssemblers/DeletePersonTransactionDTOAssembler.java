package switch2019.project.applicationLayer.dtosAssemblers;

import switch2019.project.applicationLayer.dtos.DeletePersonTransactionDTO;

public class DeletePersonTransactionDTOAssembler {

    private DeletePersonTransactionDTOAssembler() {
    }

    public static DeletePersonTransactionDTO createDTOFromPrimitiveTypes(int transactionNumber, String email) {
        DeletePersonTransactionDTO deletePersonTransactionDTO = new DeletePersonTransactionDTO(transactionNumber, email);
        return deletePersonTransactionDTO;

    }

}
