package switch2019.project.infrastructureLayer.dataPersistence.repositoriesJPA;


import org.springframework.data.repository.CrudRepository;
import switch2019.project.domainLayer.domainEntities.vosShared.GroupID;
import switch2019.project.infrastructureLayer.dataPersistence.dataModel.GroupJpa;

import java.util.List;
import java.util.Optional;

public interface GroupJpaRepository extends CrudRepository<GroupJpa, GroupID> {

	List<GroupJpa> findAll();

	Optional<GroupJpa> findById(GroupID id);

	boolean existsById(GroupID id);

	long count();
}