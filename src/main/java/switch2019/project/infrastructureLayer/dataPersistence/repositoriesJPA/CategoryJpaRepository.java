package switch2019.project.infrastructureLayer.dataPersistence.repositoriesJPA;

import org.springframework.data.repository.CrudRepository;
import switch2019.project.domainLayer.domainEntities.aggregates.category.Category;
import switch2019.project.infrastructureLayer.dataPersistence.dataModel.AbstractIdJpa;
import switch2019.project.infrastructureLayer.dataPersistence.dataModel.CategoryJpa;

import java.util.List;
import java.util.Optional;

public interface CategoryJpaRepository extends CrudRepository<CategoryJpa, AbstractIdJpa> {

	List<CategoryJpa> findAll();

	Optional<CategoryJpa> findById(AbstractIdJpa id);

	boolean existsById(AbstractIdJpa id);

	long count();

	void delete(CategoryJpa categoryJpa);

	List<CategoryJpa> findAllById(AbstractIdJpa id);
}
