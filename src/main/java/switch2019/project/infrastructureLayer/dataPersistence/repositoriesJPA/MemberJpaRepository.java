package switch2019.project.infrastructureLayer.dataPersistence.repositoriesJPA;


import org.springframework.data.repository.CrudRepository;
import switch2019.project.infrastructureLayer.dataPersistence.dataModel.MemberJpa;

import java.util.List;

public interface MemberJpaRepository extends CrudRepository<MemberJpa, Long> {

    MemberJpa findById(long id);
    //List<MemberJpa> findAllByGroupId( GroupId id);
    List<MemberJpa> findAll();
}
