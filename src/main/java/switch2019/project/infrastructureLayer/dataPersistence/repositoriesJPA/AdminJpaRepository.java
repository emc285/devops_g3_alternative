package switch2019.project.infrastructureLayer.dataPersistence.repositoriesJPA;


import org.springframework.data.repository.CrudRepository;
import switch2019.project.infrastructureLayer.dataPersistence.dataModel.AdminJpa;

import java.util.List;

public interface AdminJpaRepository extends CrudRepository<AdminJpa, Long> {

    AdminJpa findById(long id);
    //List<AdminJpa> findAllByGroupId( GroupId id);
    List<AdminJpa> findAll();
}
