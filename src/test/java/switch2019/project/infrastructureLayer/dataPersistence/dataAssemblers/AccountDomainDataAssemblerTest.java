package switch2019.project.infrastructureLayer.dataPersistence.dataAssemblers;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import switch2019.project.domainLayer.domainEntities.aggregates.account.Account;
import switch2019.project.domainLayer.domainEntities.vosShared.PersonID;
import switch2019.project.domainLayer.entitiesInterfaces.OwnerID;
import switch2019.project.infrastructureLayer.dataPersistence.dataModel.AccountJpa;

import static org.junit.jupiter.api.Assertions.assertEquals;


class AccountDomainDataAssemblerTest {

    @Test
    @DisplayName("AccountDomainDataAssembler - Test create AccountJpa to data")

    public void accountDomainDataAssembler_toData () {

        //Arrange

        String id = "alexandre@gmail.com";
        OwnerID ownerID = PersonID.createPersonID(id);
        String denomination = "Company";
        String descritpion = "Company account";

        Account account = Account.createAccount(descritpion, denomination, ownerID);

        //Act

        AccountDomainDataAssembler accountDomainDataAssembler = new AccountDomainDataAssembler();
        AccountJpa accountJpa = accountDomainDataAssembler.toData(account);

        //Assert

        assertEquals(id, accountJpa.getId().getOwnerID());
        assertEquals(denomination, accountJpa.getId().getDenomination());
        assertEquals(descritpion, accountJpa.getDescription());
    }

    @Test
    @DisplayName("AccountDomainDataAssembler - Test create AccountJpa to domain")

    public void accountDomainDataAssembler_toDomain () {

        //Arrange

        String id = "alexandre@gmail.com";
        OwnerID ownerID = PersonID.createPersonID(id);
        String denomination = "Company";
        String descritpion = "Company account";

        Account account = Account.createAccount(descritpion, denomination, ownerID);

        //Act

        AccountDomainDataAssembler accountDomainDataAssembler = new AccountDomainDataAssembler();
        AccountJpa accountJpa = accountDomainDataAssembler.toData(account);
        Account newAccount = accountDomainDataAssembler.toDomain(accountJpa);

        //Assert
        assertEquals(ownerID, newAccount.getAccountID().getOwnerID());
        assertEquals(denomination, newAccount.getAccountID().getDenomination().getDenomination());
        assertEquals(descritpion, newAccount.getDescription().getDescription());
    }

}