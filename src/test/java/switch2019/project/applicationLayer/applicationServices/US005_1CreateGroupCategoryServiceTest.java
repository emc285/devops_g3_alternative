package switch2019.project.applicationLayer.applicationServices;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import switch2019.project.applicationLayer.dtos.CreateGroupCategoryDTO;
import switch2019.project.applicationLayer.dtos.GroupDTO;
import switch2019.project.applicationLayer.dtosAssemblers.CreateGroupCategoryDTOAssembler;
import switch2019.project.applicationLayer.dtosAssemblers.GroupDTOAssembler;
import switch2019.project.controllerLayer.integrationTests.AbstractTest;
import switch2019.project.domainLayer.domainEntities.aggregates.group.Group;
import switch2019.project.domainLayer.domainEntities.aggregates.person.Person;
import switch2019.project.domainLayer.domainEntities.vosShared.*;
import switch2019.project.domainLayer.exceptions.InvalidArgumentsBusinessException;
import switch2019.project.domainLayer.exceptions.NotFoundArgumentsBusinessException;
import switch2019.project.domainLayer.repositoriesInterfaces.ICategoryRepository;
import switch2019.project.domainLayer.repositoriesInterfaces.IGroupRepository;

import java.time.LocalDate;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;


class US005_1CreateGroupCategoryServiceTest extends AbstractTest {

    @Mock
    private IGroupRepository groupRepository;
    @Mock
    private ICategoryRepository categoryRepository;

    private US005_1CreateGroupCategoryService us005_1CreateGroupCategoryService;

    private Group fontesFamily;
    private GroupID fontesFamilyID;

    @BeforeEach
    public void init() {

        //Manuel
        String manuelEmail = "manuel@gmail.com";
        PersonID manuelPersonID = PersonID.createPersonID(manuelEmail);


        //Ilda
        String ildaEmail = "ilda@gmail.com";
        PersonID ildaPersonID = PersonID.createPersonID(ildaEmail);

        //Paulo
        String pauloEmail = "paulo@gmail.com";
        PersonID pauloPersonID = PersonID.createPersonID(pauloEmail);

        //Helder
        String helderEmail = "helder@gmail.com";
        PersonID helderPersonID = PersonID.createPersonID(helderEmail);


        //Family Fontes
        //Categories ->          Salary / Draw Money / IRS / Food / Water Bill / Netflix


        String dateOfCreation = "2020-06-01";
        String fontesFamilyDenomination = "Fontes Family";
        String fontesFamilyDescription = "All members from Fontes family";

        this.fontesFamily = Group.createGroup(fontesFamilyDenomination, fontesFamilyDescription, dateOfCreation, manuelPersonID);
        this.fontesFamilyID = GroupID.createGroupID(fontesFamilyDenomination);
        fontesFamily.addPersonInCharge(ildaPersonID);
        fontesFamily.addMember(pauloPersonID);
        fontesFamily.addMember(helderPersonID);

        //Salary

        String salaryDenomination = "Salary";
        CategoryID salaryID = CategoryID.createCategoryID(salaryDenomination, fontesFamilyID);
        fontesFamily.addCategory(salaryID);

        //Draw Money

        String drawMoneyDenomination = "Draw Money";
        CategoryID drawMoneyID = CategoryID.createCategoryID(drawMoneyDenomination, fontesFamilyID);
        fontesFamily.addCategory(drawMoneyID);

        //IRS

        String irsDenomination = "IRS";
        CategoryID irsID = CategoryID.createCategoryID(irsDenomination, fontesFamilyID);
        fontesFamily.addCategory(irsID);

        //Food

        String foodDenomination = "Food";
        CategoryID foodID = CategoryID.createCategoryID(foodDenomination, fontesFamilyID);
        fontesFamily.addCategory(foodID);


        //Water Bill

        String waterBillDenomination = "Water Bill";
        CategoryID waterBillID = CategoryID.createCategoryID(waterBillDenomination, fontesFamilyID);
        fontesFamily.addCategory(waterBillID);


        //Netflix

        String netflixDenomination = "Netflix";
        CategoryID netflixID = CategoryID.createCategoryID(netflixDenomination, fontesFamilyID);
        fontesFamily.addCategory(netflixID);

    }

    //Tests

    @Test
    @DisplayName("Test For createCategoryAsPeopleInCharge() | Fontes Family | Equipment | Success")
    void createCategoryAsPeopleInCharge_FontesFamily_Success() {

        // Arrange


        String personEmail = "manuel@gmail.com";
        String groupDenomination = "Fontes Family";
        String groupDescription = "All members from Fontes family";
        LocalDate dateOfCreation = LocalDate.of(2020, 06, 01);

        String categoryDenomination = "Equipment";

        //To Search
        CategoryID categoryID = CategoryID.createCategoryID(categoryDenomination, fontesFamilyID);

        //Returning an Optional<Group> Fontes Family
        Mockito.when(groupRepository.findById(fontesFamilyID)).thenReturn(Optional.of(fontesFamily));

        //Returning False
        Mockito.when(categoryRepository.existsById(categoryID)).thenReturn(false);

        //DTO
        CreateGroupCategoryDTO createGroupCategoryDTO = CreateGroupCategoryDTOAssembler.createDTOFromPrimitiveTypes(personEmail, groupDenomination, categoryDenomination);

        //Expected GroupDTO
        GroupDTO expectedGroupDTO = GroupDTOAssembler.createDTOFromDomainObject(Denomination.createDenomination(groupDenomination), Description.createDescription(groupDescription), DateOfCreation.createDateOfCreation(dateOfCreation));

        //Service
        us005_1CreateGroupCategoryService = new US005_1CreateGroupCategoryService(groupRepository, categoryRepository);


        // Act

        GroupDTO result = us005_1CreateGroupCategoryService.createCategoryAsPeopleInCharge(createGroupCategoryDTO);


        // Assert

        assertEquals(expectedGroupDTO, result);
    }

    @Test
    @DisplayName("Test For createCategoryAsPeopleInCharge() | Fontes Family | Equipment | Fail | Person Not In Charge")
    void createCategoryAsPeopleInCharge_Runners_Fail_PeopleNotInCharge() {

        // Arrange

        String personEmail = "paulo@gmail.com";
        String groupDenomination = "Fontes Family";
        String categoryDenomination = "Equipment";

        //Returning an Optional<Group> Fontes Family
        Mockito.when(groupRepository.findById(fontesFamilyID)).thenReturn(Optional.of(fontesFamily));

        //DTO
        CreateGroupCategoryDTO createGroupCategoryDTO = CreateGroupCategoryDTOAssembler.createDTOFromPrimitiveTypes(personEmail, groupDenomination, categoryDenomination);

        //Service
        us005_1CreateGroupCategoryService = new US005_1CreateGroupCategoryService(groupRepository, categoryRepository);


        // Act

        Throwable thrown = assertThrows(InvalidArgumentsBusinessException.class, () -> us005_1CreateGroupCategoryService.createCategoryAsPeopleInCharge(createGroupCategoryDTO));


        //Assert

        assertEquals(thrown.getMessage(), US005_1CreateGroupCategoryService.PERSON_NOT_IN_CHARGE);
    }

    @Test
    @DisplayName("Test For createCategoryAsPeopleInCharge() | Fontes Family | Salary | Fail | Category Already Exists")
    void createCategoryAsPeopleInCharge_House_Fail_PeopleNotInCharge() {

        // Arrange

        String personEmail = "manuel@gmail.com";
        String groupDenomination = "Fontes Family";
        String categoryDenomination = "Salary";

        //To Search
        CategoryID categoryID = CategoryID.createCategoryID(categoryDenomination, fontesFamilyID);

        //Returning an Optional<Group> Fontes Family
        Mockito.when(groupRepository.findById(fontesFamilyID)).thenReturn(Optional.of(fontesFamily));

        //Returning False
        Mockito.when(categoryRepository.existsById(categoryID)).thenReturn(true);

        //DTO
        CreateGroupCategoryDTO createGroupCategoryDTO = CreateGroupCategoryDTOAssembler.createDTOFromPrimitiveTypes(personEmail, groupDenomination, categoryDenomination);

        //Service
        us005_1CreateGroupCategoryService = new US005_1CreateGroupCategoryService(groupRepository, categoryRepository);


        // Act

        Throwable thrown = assertThrows(InvalidArgumentsBusinessException.class, () -> us005_1CreateGroupCategoryService.createCategoryAsPeopleInCharge(createGroupCategoryDTO));


        //Assert

        assertEquals(thrown.getMessage(), US005_1CreateGroupCategoryService.CATEGORY_ALREADY_EXIST);
    }

    @Test
    @DisplayName("Test For createCategoryAsPeopleInCharge() | Fontes Family | Equipment | Fail | Group Does Not Exist")
    void createCategoryAsPeopleInCharge_Runners_Fail_GroupDoesNotExist() {

        // Arrange

        String personEmail = "paulo@gmail.com";
        String groupDenomination = "Friends";
        String categoryDenomination = "Equipment";

        //Returning an Optional<Group> Fontes Family
        Mockito.when(groupRepository.findById(fontesFamilyID)).thenReturn(Optional.empty());

        //DTO
        CreateGroupCategoryDTO createGroupCategoryDTO = CreateGroupCategoryDTOAssembler.createDTOFromPrimitiveTypes(personEmail, groupDenomination, categoryDenomination);

        //Service
        us005_1CreateGroupCategoryService = new US005_1CreateGroupCategoryService(groupRepository, categoryRepository);


        // Act

        Throwable thrown = assertThrows(NotFoundArgumentsBusinessException.class, () -> us005_1CreateGroupCategoryService.createCategoryAsPeopleInCharge(createGroupCategoryDTO));


        //Assert

        assertEquals(thrown.getMessage(), US005_1CreateGroupCategoryService.GROUP_DOES_NOT_EXIST);
    }

}