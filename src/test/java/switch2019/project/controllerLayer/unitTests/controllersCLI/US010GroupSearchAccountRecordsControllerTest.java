package switch2019.project.controllerLayer.unitTests.controllersCLI;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import switch2019.project.applicationLayer.applicationServices.US010GroupSearchAccountRecordsService;
import switch2019.project.applicationLayer.dtos.GroupSearchAccountRecordsInDTO;
import switch2019.project.applicationLayer.dtos.SearchAccountRecordsOutDTO;
import switch2019.project.applicationLayer.dtosAssemblers.GroupSearchAccountRecordsInDTOAssembler;
import switch2019.project.applicationLayer.dtosAssemblers.SearchAccountRecordsOutDTOAssembler;
import switch2019.project.controllerLayer.controllers.controllersCLI.US010GroupSearchAccountRecordsController;
import switch2019.project.controllerLayer.integrationTests.AbstractTest;
import switch2019.project.domainLayer.domainEntities.aggregates.ledger.Transaction;
import switch2019.project.domainLayer.domainEntities.vosShared.AccountID;
import switch2019.project.domainLayer.domainEntities.vosShared.CategoryID;
import switch2019.project.domainLayer.domainEntities.vosShared.GroupID;
import switch2019.project.domainLayer.exceptions.InvalidArgumentsBusinessException;
import switch2019.project.domainLayer.exceptions.NotFoundArgumentsBusinessException;

import java.time.LocalDate;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class US010GroupSearchAccountRecordsControllerTest extends AbstractTest {

    @Mock
    private US010GroupSearchAccountRecordsService us010GroupService;

    @Test
    @DisplayName("Test getGroupAccountTransactionsWithinPeriod - Success")
    void getGroupAccountTransactionsWithinPeriod_success() {

        //ARRANGE
        //Info for DTO in
        String personEmail = "maria@gmail.com";
        String groupDenomination = "House mattes";
        String accountDenomination = "EDP";
        String startDate = "2020-03-03";
        String endDate = "2020-04-03";

        //DTO in
        GroupSearchAccountRecordsInDTO dtoIn = GroupSearchAccountRecordsInDTOAssembler.groupSearchAccountRecordsInDTO(personEmail, groupDenomination, accountDenomination, startDate, endDate);

        //Arrange category and accounts for expected transactions
        //Category Electricity Expenses
        String denominationCategoryEDP = "Electricity Expenses";
        CategoryID categoryIDEdp = CategoryID.createCategoryID(denominationCategoryEDP, GroupID.createGroupID(groupDenomination));

        //Account EDP
        AccountID accountIDEdp = AccountID.createAccountID(accountDenomination, GroupID.createGroupID(groupDenomination));

        //Account Wallet - House expenses
        String walletAccountDenomination = "House Wallet Funds";
        AccountID accountIDWallet = AccountID.createAccountID(walletAccountDenomination, GroupID.createGroupID(groupDenomination));

        //Arrange expected transactions
        //Transaction 2 - EDP - Debit
        String typeTransaction2 = "Debit";
        String descriptionTransaction2 = "EDP bill from February/2020";
        LocalDate dateTransaction2 = LocalDate.of(2020, 03, 03);
        double amountTransaction2 = 45.00;
        Transaction transaction2 = Transaction.createTransaction(categoryIDEdp, typeTransaction2, descriptionTransaction2, amountTransaction2, dateTransaction2, accountIDWallet, accountIDEdp);

        //Transaction 4 - EDP - Credit
        String typeTransaction4 = "Credit";
        String descriptionTransaction4 = "EDP bill from March/2020 - settlement - overcharge";
        LocalDate dateTransaction4 = LocalDate.of(2020, 04, 03);
        double amountTransaction4 = 15.00;
        Transaction transaction4 = Transaction.createTransaction(categoryIDEdp, typeTransaction4, descriptionTransaction4, amountTransaction4, dateTransaction4, accountIDEdp, accountIDWallet);

        //Expected DTO out
        ArrayList<Transaction> expectedTransactions = new ArrayList<>();
        expectedTransactions.add(transaction2);
        expectedTransactions.add(transaction4);

        SearchAccountRecordsOutDTO expectedOutDTO = SearchAccountRecordsOutDTOAssembler.accountTransactionsOutDTO(expectedTransactions);

        //Mock the behaviour of the service method getGroupAccountTransactionsWithinPeriod,
        //so it does not depend on other parts (e.g. DB)
        Mockito.when(us010GroupService.getGroupAccountTransactionsWithinPeriod(dtoIn)).thenReturn(expectedOutDTO);

        //Instantiate controller
        US010GroupSearchAccountRecordsController us010GroupController = new US010GroupSearchAccountRecordsController(us010GroupService);

        //ACT
        SearchAccountRecordsOutDTO result = us010GroupController.getGroupAccountTransactionsWithinPeriod(personEmail, groupDenomination, accountDenomination, startDate, endDate);

        //ASSERT
        assertEquals(expectedOutDTO, result);

    }

    @Test
    @DisplayName("Test for getGroupAccountTransactionsWithinPeriod() - Exception - Group does not exist")
    void getGroupAccountTransactionsWithinPeriod_exception_groupDoesntExist() {

        //ARRANGE
        //Info for DTO in
        String personEmail = "maria@gmail.com";
        String groupDenomination = "Swimming mattes";
        String accountDenomination = "EDP";
        String startDate = "2020-03-03";
        String endDate = "2020-04-03";

        //DTO in
        GroupSearchAccountRecordsInDTO dtoIn = GroupSearchAccountRecordsInDTOAssembler.groupSearchAccountRecordsInDTO(personEmail, groupDenomination, accountDenomination, startDate, endDate);

        String expectedMessage = "Group does not exist in the system";

        //Mock the behaviour of the service method getGroupAccountTransactionsWithinPeriod,
        //so it does not depend on other parts (e.g. DB)
        Mockito.when(us010GroupService.getGroupAccountTransactionsWithinPeriod(dtoIn)).thenThrow(new InvalidArgumentsBusinessException(US010GroupSearchAccountRecordsService.GROUP_DOES_NOT_EXIST));

        //Instantiate controller
        US010GroupSearchAccountRecordsController us010GroupController = new US010GroupSearchAccountRecordsController(us010GroupService);

        //ACT
        Throwable thrown = assertThrows(InvalidArgumentsBusinessException.class, () -> us010GroupController.getGroupAccountTransactionsWithinPeriod(personEmail, groupDenomination, accountDenomination, startDate, endDate));

        //ASSERT
        assertEquals(expectedMessage, thrown.getMessage());

    }


    @Test
    @DisplayName("Test for getGroupAccountTransactionsWithinPeriod() - Exception - Person is not member")
    void getGroupAccountTransactionsWithinPeriod_exception_personNotMember() {

        //ARRANGE
        //Info for DTO in
        String personEmail = "josefina@gmail.com";
        String groupDenomination = "House mattes";
        String accountDenomination = "EDP";
        String startDate = "2020-03-03";
        String endDate = "2020-04-03";

        //DTO in
        GroupSearchAccountRecordsInDTO dtoIn = GroupSearchAccountRecordsInDTOAssembler.groupSearchAccountRecordsInDTO(personEmail, groupDenomination, accountDenomination, startDate, endDate);

        String expectedMessage = "Person is not member of the group";

        //Mock the behaviour of the service method getGroupAccountTransactionsWithinPeriod,
        //so it does not depend on other parts (e.g. DB)
        Mockito.when(us010GroupService.getGroupAccountTransactionsWithinPeriod(dtoIn)).thenThrow(new InvalidArgumentsBusinessException(US010GroupSearchAccountRecordsService.PERSON_NOT_MEMBER));

        //Instantiate controller
        US010GroupSearchAccountRecordsController us010GroupController = new US010GroupSearchAccountRecordsController(us010GroupService);

        //ACT
        Throwable thrown = assertThrows(InvalidArgumentsBusinessException.class, () -> us010GroupController.getGroupAccountTransactionsWithinPeriod(personEmail, groupDenomination, accountDenomination, startDate, endDate));

        //ASSERT
        assertEquals(expectedMessage, thrown.getMessage());

    }


    @Test
    @DisplayName("Test for getGroupAccountTransactionsWithinPeriod() - Exception - Account does not exist")
    void getGroupAccountTransactionsWithinPeriod_exception_accountDoesntExist() {

        //ARRANGE
        //Info for DTO in
        String personEmail = "maria@gmail.com";
        String groupDenomination = "House mattes";
        String accountDenomination = "Pool";
        String startDate = "2020-03-03";
        String endDate = "2020-04-03";

        //DTO in
        GroupSearchAccountRecordsInDTO dtoIn = GroupSearchAccountRecordsInDTOAssembler.groupSearchAccountRecordsInDTO(personEmail, groupDenomination, accountDenomination, startDate, endDate);

        //Expected message
        String expectedMessage = "Account does not exist in the system";

        //Mock the behaviour of the service method getGroupAccountTransactionsWithinPeriod,
        //so it does not depend on other parts (e.g. DB)
        Mockito.when(us010GroupService.getGroupAccountTransactionsWithinPeriod(dtoIn)).thenThrow(new InvalidArgumentsBusinessException(US010GroupSearchAccountRecordsService.ACCOUNT_DOES_NOT_EXIST));

        //Instantiate controller
        US010GroupSearchAccountRecordsController us010GroupController = new US010GroupSearchAccountRecordsController(us010GroupService);

        //ACT
        Throwable thrown = assertThrows(InvalidArgumentsBusinessException.class, () -> us010GroupController.getGroupAccountTransactionsWithinPeriod(personEmail, groupDenomination, accountDenomination, startDate, endDate));

        //ASSERT
        assertEquals(expectedMessage, thrown.getMessage());

    }

    @Test
    @DisplayName("Test for getGroupAccountTransactionsWithinPeriod() - Exception - Start and end dates order")
    void getGroupAccountTransactionsWithinPeriod_exception_searchDatesOrder() {

        //ARRANGE
        //Info for DTO in
        String personEmail = "maria@gmail.com";
        String groupDenomination = "House mattes";
        String accountDenomination = "EDP";
        String startDate = "2020-05-03";
        String endDate = "2020-04-03";

        //DTO in
        GroupSearchAccountRecordsInDTO dtoIn = GroupSearchAccountRecordsInDTOAssembler.groupSearchAccountRecordsInDTO(personEmail, groupDenomination, accountDenomination, startDate, endDate);

        //Expected message
        String expectedMessage = "Check the start and end dates for the period, since start date cannot be later than end date";

        //Mock the behaviour of the service method getGroupAccountTransactionsWithinPeriod,
        //so it does not depend on other parts (e.g. DB)
        Mockito.when(us010GroupService.getGroupAccountTransactionsWithinPeriod(dtoIn)).thenThrow(new InvalidArgumentsBusinessException(US010GroupSearchAccountRecordsService.DATES_IN_REVERSE_ORDER));

        //Instantiate controller
        US010GroupSearchAccountRecordsController us010GroupController = new US010GroupSearchAccountRecordsController(us010GroupService);

        //ACT
        Throwable thrown = assertThrows(InvalidArgumentsBusinessException.class, () -> us010GroupController.getGroupAccountTransactionsWithinPeriod(personEmail, groupDenomination, accountDenomination, startDate, endDate));

        //ASSERT
        assertEquals(expectedMessage, thrown.getMessage());
    }

    @Test
    @DisplayName("Test for getGroupAccountTransactionsWithinPeriod() - Exception - Search period prior to ledger records")
    void getGroupAccountTransactionsWithinPeriod_exception_searchBeforeLedgerRecords() {

        //ARRANGE
        //Info for DTO in
        String personEmail = "maria@gmail.com";
        String groupDenomination = "House mattes";
        String accountDenomination = "EDP";
        String startDate = "2020-01-01";
        String endDate = "2020-01-20";

        //DTO in
        GroupSearchAccountRecordsInDTO dtoIn = GroupSearchAccountRecordsInDTOAssembler.groupSearchAccountRecordsInDTO(personEmail, groupDenomination, accountDenomination, startDate, endDate);

        //Expected message
        String expectedMessage = "The time period provided falls outside the range of the ledger records";

        //Mock the behaviour of the service method getGroupAccountTransactionsWithinPeriod,
        //so it does not depend on other parts (e.g. DB)
        Mockito.when(us010GroupService.getGroupAccountTransactionsWithinPeriod(dtoIn)).thenThrow(new NotFoundArgumentsBusinessException(US010GroupSearchAccountRecordsService.TIME_PERIOD_OUTSIDE_OF_RECORDS_RANGE));

        //Instantiate controller
        US010GroupSearchAccountRecordsController us010GroupController = new US010GroupSearchAccountRecordsController(us010GroupService);

        //ACT
        Throwable thrown = assertThrows(NotFoundArgumentsBusinessException.class, () -> us010GroupController.getGroupAccountTransactionsWithinPeriod(personEmail, groupDenomination, accountDenomination, startDate, endDate));

        //ASSERT
        assertEquals(expectedMessage, thrown.getMessage());
    }

    @Test
    @DisplayName("Test for getGroupAccountTransactionsWithinPeriod() - Exception - Search period after ledger records")
    void getGroupAccountTransactionsWithinPeriod_exception_searchAfterLedgerRecords() {

        //ARRANGE
        //Info for DTO in
        String personEmail = "maria@gmail.com";
        String groupDenomination = "House mattes";
        String accountDenomination = "EDP";
        String startDate = "2020-06-01";
        String endDate = "2020-06-10";

        //DTO in
        GroupSearchAccountRecordsInDTO dtoIn = GroupSearchAccountRecordsInDTOAssembler.groupSearchAccountRecordsInDTO(personEmail, groupDenomination, accountDenomination, startDate, endDate);

        //Expected message
        String expectedMessage = "The time period provided falls outside the range of the ledger records";

        //Mock the behaviour of the service method getGroupAccountTransactionsWithinPeriod,
        //so it does not depend on other parts (e.g. DB)
        Mockito.when(us010GroupService.getGroupAccountTransactionsWithinPeriod(dtoIn)).thenThrow(new NotFoundArgumentsBusinessException(US010GroupSearchAccountRecordsService.TIME_PERIOD_OUTSIDE_OF_RECORDS_RANGE));

        //Instantiate controller
        US010GroupSearchAccountRecordsController us010GroupController = new US010GroupSearchAccountRecordsController(us010GroupService);

        //ACT
        Throwable thrown = assertThrows(NotFoundArgumentsBusinessException.class, () -> us010GroupController.getGroupAccountTransactionsWithinPeriod(personEmail, groupDenomination, accountDenomination, startDate, endDate));

        //ASSERT
        assertEquals(expectedMessage, thrown.getMessage());
    }

    @Test
    @DisplayName("Test for getGroupAccountTransactionsWithinPeriod() - Exception - No transactions within the search period")
    void getGroupAccountTransactionsWithinPeriod_exception_noTransactionsInSearchPeriod() {

        //ARRANGE
        //Info for DTO in
        String personEmail = "maria@gmail.com";
        String groupDenomination = "House mattes";
        String accountDenomination = "EDP";
        String startDate = "2020-03-04";
        String endDate = "2020-04-02";

        //DTO in
        GroupSearchAccountRecordsInDTO dtoIn = GroupSearchAccountRecordsInDTOAssembler.groupSearchAccountRecordsInDTO(personEmail, groupDenomination, accountDenomination, startDate, endDate);

        //Expected message
        String expectedMessage = "Ledger has no transactions within the searched period";

        //Mock the behaviour of the service method getGroupAccountTransactionsWithinPeriod,
        //so it does not depend on other parts (e.g. DB)
        Mockito.when(us010GroupService.getGroupAccountTransactionsWithinPeriod(dtoIn)).thenThrow(new NotFoundArgumentsBusinessException(US010GroupSearchAccountRecordsService.NO_TRANSACTIONS_TO_REPORT));

        //Instantiate controller
        US010GroupSearchAccountRecordsController us010GroupController = new US010GroupSearchAccountRecordsController(us010GroupService);

        //ACT
        Throwable thrown = assertThrows(NotFoundArgumentsBusinessException.class, () -> us010GroupController.getGroupAccountTransactionsWithinPeriod(personEmail, groupDenomination, accountDenomination, startDate, endDate));

        //ASSERT
        assertEquals(expectedMessage, thrown.getMessage());
    }

    @Test
    @DisplayName("Test for getGroupAccountTransactionsWithinPeriod() - Exception - Ledger empty")
    void getGroupAccountTransactionsWithinPeriod_exception_emptyLedger() {

        //ARRANGE
        //Info for DTO in
        String personEmail = "maria@gmail.com";
        String groupDenomination = "House mattes";
        String accountDenomination = "EDP";
        String startDate = "2020-03-04";
        String endDate = "2020-04-02";

        //DTO in
        GroupSearchAccountRecordsInDTO dtoIn = GroupSearchAccountRecordsInDTOAssembler.groupSearchAccountRecordsInDTO(personEmail, groupDenomination, accountDenomination, startDate, endDate);

        //Expected message
        String expectedMessage = "Ledger is empty";

        //Mock the behaviour of the service method getGroupAccountTransactionsWithinPeriod,
        //so it does not depend on other parts (e.g. DB)
        Mockito.when(us010GroupService.getGroupAccountTransactionsWithinPeriod(dtoIn)).thenThrow(new NotFoundArgumentsBusinessException(US010GroupSearchAccountRecordsService.EMPTY_LEDGER));

        //Instantiate controller
        US010GroupSearchAccountRecordsController us010GroupController = new US010GroupSearchAccountRecordsController(us010GroupService);

        //ACT
        Throwable thrown = assertThrows(NotFoundArgumentsBusinessException.class, () -> us010GroupController.getGroupAccountTransactionsWithinPeriod(personEmail, groupDenomination, accountDenomination, startDate, endDate));

        //ASSERT
        assertEquals(expectedMessage, thrown.getMessage());
    }

    @Test
    @DisplayName("Test for getGroupAccountTransactionsWithinPeriod() - Exception - Account denomination is missing")
    void getGroupAccountTransactionsWithinPeriod_exception_accountNameMissing() {

        //ARRANGE
        //Info for DTO in
        String personEmail = "maria@gmail.com";
        String groupDenomination = "House mattes";
        String accountDenomination = "";
        String startDate = "2020-03-03";
        String endDate = "2020-04-03";

        //DTO in
        GroupSearchAccountRecordsInDTO dtoIn = GroupSearchAccountRecordsInDTOAssembler.groupSearchAccountRecordsInDTO(personEmail, groupDenomination, accountDenomination, startDate, endDate);

        //Expected message
        String expectedMessage = "Search results cannot be displayed: account name is missing";

        //Mock the behaviour of the service method getGroupAccountTransactionsWithinPeriod,
        //so it does not depend on other parts (e.g. DB)
        Mockito.when(us010GroupService.getGroupAccountTransactionsWithinPeriod(dtoIn)).thenThrow(new NotFoundArgumentsBusinessException(US010GroupSearchAccountRecordsService.ACCOUNT_NAME_FIELD_MISSING));

        //Instantiate controller
        US010GroupSearchAccountRecordsController us010GroupController = new US010GroupSearchAccountRecordsController(us010GroupService);

        //ACT
        Throwable thrown = assertThrows(NotFoundArgumentsBusinessException.class, () -> us010GroupController.getGroupAccountTransactionsWithinPeriod(personEmail, groupDenomination, accountDenomination, startDate, endDate));

        //ASSERT
        assertEquals(expectedMessage, thrown.getMessage());
    }

    @Test
    @DisplayName("Test for getGroupAccountTransactionsWithinPeriod() - Exception - Start date is missing")
    void getGroupAccountTransactionsWithinPeriod_exception_startDateMissing() {

        //ARRANGE
        //Info for DTO in
        String personEmail = "maria@gmail.com";
        String groupDenomination = "House mattes";
        String accountDenomination = "EDP";
        String startDate = "";
        String endDate = "2020-04-03";

        //DTO in
        GroupSearchAccountRecordsInDTO dtoIn = GroupSearchAccountRecordsInDTOAssembler.groupSearchAccountRecordsInDTO(personEmail, groupDenomination, accountDenomination, startDate, endDate);

        //Expected message
        String expectedMessage = "Search results cannot be displayed: start date is missing";

        //Mock the behaviour of the service method getGroupAccountTransactionsWithinPeriod,
        //so it does not depend on other parts (e.g. DB)
        Mockito.when(us010GroupService.getGroupAccountTransactionsWithinPeriod(dtoIn)).thenThrow(new NotFoundArgumentsBusinessException(US010GroupSearchAccountRecordsService.START_DATE_FIELD_MISSING));

        //Instantiate controller
        US010GroupSearchAccountRecordsController us010GroupController = new US010GroupSearchAccountRecordsController(us010GroupService);

        //ACT
        Throwable thrown = assertThrows(NotFoundArgumentsBusinessException.class, () -> us010GroupController.getGroupAccountTransactionsWithinPeriod(personEmail, groupDenomination, accountDenomination, startDate, endDate));

        //ASSERT
        assertEquals(expectedMessage, thrown.getMessage());
    }

    @Test
    @DisplayName("Test for getGroupAccountTransactionsWithinPeriod() - Exception - End date is missing")
    void getGroupAccountTransactionsWithinPeriod_exception_endDateMissing() {

        //ARRANGE
        //Info for DTO in
        String personEmail = "maria@gmail.com";
        String groupDenomination = "House mattes";
        String accountDenomination = "EDP";
        String startDate = "2020-03-03";
        String endDate = "";


        //DTO in
        GroupSearchAccountRecordsInDTO dtoIn = GroupSearchAccountRecordsInDTOAssembler.groupSearchAccountRecordsInDTO(personEmail, groupDenomination, accountDenomination, startDate, endDate);

        //Expected message
        String expectedMessage = "Search results cannot be displayed: end date is missing";

        //Mock the behaviour of the service method getGroupAccountTransactionsWithinPeriod,
        //so it does not depend on other parts (e.g. DB)
        Mockito.when(us010GroupService.getGroupAccountTransactionsWithinPeriod(dtoIn)).thenThrow(new NotFoundArgumentsBusinessException(US010GroupSearchAccountRecordsService.END_DATE_FIELD_MISSING));

        //Instantiate controller
        US010GroupSearchAccountRecordsController us010GroupController = new US010GroupSearchAccountRecordsController(us010GroupService);

        //ACT
        Throwable thrown = assertThrows(NotFoundArgumentsBusinessException.class, () -> us010GroupController.getGroupAccountTransactionsWithinPeriod(personEmail, groupDenomination, accountDenomination, startDate, endDate));

        //ASSERT
        assertEquals(expectedMessage, thrown.getMessage());
    }

}
