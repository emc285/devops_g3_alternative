package switch2019.project.controllerLayer.unitTests.controllersCLI;


import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import switch2019.project.applicationLayer.applicationServices.US007CreateGroupAccountService;
import switch2019.project.applicationLayer.dtos.CreateGroupAccountDTO;
import switch2019.project.applicationLayer.dtos.GroupDTO;
import switch2019.project.applicationLayer.dtosAssemblers.CreateGroupAccountDTOAssembler;
import switch2019.project.applicationLayer.dtosAssemblers.GroupDTOAssembler;
import switch2019.project.controllerLayer.controllers.controllersCLI.US007CreateGroupAccountController;
import switch2019.project.controllerLayer.integrationTests.AbstractTest;
import switch2019.project.domainLayer.domainEntities.vosShared.DateOfCreation;
import switch2019.project.domainLayer.domainEntities.vosShared.Denomination;
import switch2019.project.domainLayer.domainEntities.vosShared.Description;
import switch2019.project.domainLayer.exceptions.InvalidArgumentsBusinessException;
import switch2019.project.domainLayer.exceptions.NotFoundArgumentsBusinessException;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class US007CreateGroupAccountControllerTest extends AbstractTest {

    @Mock
    private US007CreateGroupAccountService service;
    
    //SUCCESS

    @Test
    @DisplayName("Test For createAccountAsPeopleInCharge() | Success")
    public void whenGroupAccountIsCreated_MsgSuccess() {

        //Arrange
        String personEmail = "ilda@gmail.com";
        String groupDenomination = "Fontes Family";
        String groupDescription = "All members from Fontes family";
        String accountDenomination = "Allowance";
        String accountDescription = "Lakers Expenses";

        //Expected result
        Denomination denomination = Denomination.createDenomination(groupDenomination);
        Description description = Description.createDescription(groupDescription);
        DateOfCreation dateOfCreation = DateOfCreation.createDateOfCreation(LocalDate.now());

        GroupDTO isAccountCreatedExpected = GroupDTOAssembler.createDTOFromDomainObject(denomination, description, dateOfCreation);

        CreateGroupAccountDTO createGroupAccountDTO = CreateGroupAccountDTOAssembler.createDTOFromPrimitiveTypes(personEmail, groupDenomination, accountDescription, accountDenomination);

        // Mock the behaviour of the service's createAccountAsPeopleInCharge method
        Mockito.when(service.createAccountAsPeopleInCharge(createGroupAccountDTO)).thenReturn(isAccountCreatedExpected);

        //Controller
        US007CreateGroupAccountController controller = new US007CreateGroupAccountController(service);

        //Act
        GroupDTO result = controller.createAccountAsPeopleInCharge(personEmail, groupDenomination, accountDescription, accountDenomination);

        //Assert
        assertEquals(isAccountCreatedExpected, result);
    }

    //PERSON_NOT_IN_CHARGE

    @Test
    @DisplayName("Test For createAccountAsPeopleInCharge() | People Not In Charge")
    public void whenGroupAccountIsCreated_MsgPersonNotInCharge() {

        //Arrange
        String personEmail = "lebron@gmail.com";
        String groupDenomination = "Fontes Family";
        String accountDenomination = "LakersAccount";
        String accountDescription = "Lakers Expenses";

        CreateGroupAccountDTO createGroupAccountDTO = CreateGroupAccountDTOAssembler.createDTOFromPrimitiveTypes(personEmail, groupDenomination, accountDescription, accountDenomination);

        // Mock the behaviour of the service's createAccountAsPeopleInCharge method
        Mockito.when(service.createAccountAsPeopleInCharge(createGroupAccountDTO)).thenThrow(new InvalidArgumentsBusinessException(US007CreateGroupAccountService.PERSON_NOT_IN_CHARGE));

        //Controller
        US007CreateGroupAccountController controller = new US007CreateGroupAccountController(service);

        //Act
        Throwable thrown = assertThrows(InvalidArgumentsBusinessException.class, () -> controller.createAccountAsPeopleInCharge(personEmail, groupDenomination, accountDescription, accountDenomination));

        //Assert
        assertEquals(thrown.getMessage(), US007CreateGroupAccountService.PERSON_NOT_IN_CHARGE);
    }

    //ACCOUNT_ALREADY_EXIST

    @Test
    @DisplayName("Test For createAccountAsPeopleInCharge() | Account Already Exists")
    public void whenGroupAccountIsCreated_MsgAccountAlreadyExists() {

        //Arrange
        String personEmail = "ilda@gmail.com";
        String groupDenomination = "Fontes Family";
        String accountDenomination = "Company";
        String accountDescription = "Company Expenses";

        CreateGroupAccountDTO createGroupAccountDTO = CreateGroupAccountDTOAssembler.createDTOFromPrimitiveTypes(personEmail, groupDenomination, accountDescription, accountDenomination);

        // Mock the behaviour of the service's createAccountAsPeopleInCharge method
        Mockito.when(service.createAccountAsPeopleInCharge(createGroupAccountDTO)).thenThrow(new InvalidArgumentsBusinessException(US007CreateGroupAccountService.ACCOUNT_ALREADY_EXIST));

        //Controller
        US007CreateGroupAccountController controller = new US007CreateGroupAccountController(service);

        //Act
        Throwable thrown = assertThrows(InvalidArgumentsBusinessException.class, () -> controller.createAccountAsPeopleInCharge(personEmail, groupDenomination, accountDescription, accountDenomination));

        //Assert
        assertEquals(thrown.getMessage(), US007CreateGroupAccountService.ACCOUNT_ALREADY_EXIST);
    }

    //GROUP_DOES_NOT_EXIST

    @Test
    @DisplayName("Test For createAccountAsPeopleInCharge() | Group Does Not Exist")
    public void whenGroupAccountIsCreated_MsgGroupDoesNotExist() {

        //Arrange
        String personEmail = "ilda@gmail.com";
        String groupDenomination = "Lakers Family";
        String accountDenomination = "LakersAccount";
        String accountDescription = "Lakers Expenses";

        CreateGroupAccountDTO createGroupAccountDTO = CreateGroupAccountDTOAssembler.createDTOFromPrimitiveTypes(personEmail, groupDenomination, accountDescription, accountDenomination);

        // Mock the behaviour of the service's createAccountAsPeopleInCharge method
        Mockito.when(service.createAccountAsPeopleInCharge(createGroupAccountDTO)).thenThrow(new NotFoundArgumentsBusinessException(US007CreateGroupAccountService.GROUP_DOES_NOT_EXIST));

        //Controller
        US007CreateGroupAccountController controller = new US007CreateGroupAccountController(service);

        //Act
        Throwable thrown = assertThrows(NotFoundArgumentsBusinessException.class, () -> controller.createAccountAsPeopleInCharge(personEmail, groupDenomination, accountDescription, accountDenomination));

        //Assert
        assertEquals(thrown.getMessage(), US007CreateGroupAccountService.GROUP_DOES_NOT_EXIST);
    }
}